/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Adeel Khilji
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testAddProductGood() {
        System.out.println("addProduct-checkLength-Regular");
        Cart instance = new Cart();
        instance.addProduct(new Product("Shoes",60));
        instance.addProduct(new Product("Hat",10));
        boolean expResult = true;
        boolean result = instance.getCartSize();
        assertEquals("Product was successfully added to the cart",expResult,result);
    }
    
    @Test
    public void testAddProductBad() {
        System.out.println("addProduct-checkLength-Bad");
        Cart instance = new Cart();
        Product product = new Product("Shoes",60);
        instance.addProduct(product);
        boolean expResult = false;
        boolean result = instance.getCartSize();
        assertEquals("Product was successfully added to the cart",expResult,result);
    }
}
