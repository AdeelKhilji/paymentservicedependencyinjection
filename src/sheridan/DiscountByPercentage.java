/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Adeel Khilji
 */
public class DiscountByPercentage extends Discount
{
    DiscountByPercentage(){}
//    protected DiscountByPercentage(double amount)
//    {
//        super(amount);
//    }

    @Override
    public double calculateDiscount(double amount) 
    {
        return amount / 100;
    }
    
}
