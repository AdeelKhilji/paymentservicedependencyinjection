/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Adeel Khilji
 */
public class PaymentServiceFactory 
{
    private static PaymentServiceFactory paymentServiceFactory;
    
    private PaymentServiceFactory(){}
    
    public static PaymentServiceFactory getInstance()
    {
        if(paymentServiceFactory == null)
        {
            paymentServiceFactory = new PaymentServiceFactory();
        }
        return paymentServiceFactory;
    }
    
    public PaymentService getPaymentService(PaymentServiceType type)
    {
        PaymentService service = null;
        
        switch(type)
        {
            case CREDIT: service = new CreditPaymentService();
            break;
            case DEBIT: service = new DebitPaymentService();
            break;
        }
        return service;
    }
}
