/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Adeel Khilji
 */
public class Cart 
{
    private List<Product> products;
    private PaymentService service;
    
    public Cart()
    {
        products  = new ArrayList<Product>();
    }
    public void setPaymentService(PaymentService service)
    {
        this.service = service;
    }
    public void addProduct(Product product)
    {
        products.add(product);
    }
    public boolean getCartSize()
    {
        return products.size() >= 2;
    }
    public void payCart()
    {
        double totalPrice = 0;
        for(Product product: products)
        {
            totalPrice += product.getPrice();
        }
        DiscountFactory factory = DiscountFactory.getInstance();
        Discount discount = null;
        if(totalPrice > 0)
        {
            discount.calculateDiscount(totalPrice);
        }
        //add discount here $10 discount 20% discount, create a factory for discounts
        
        service.processPayment(totalPrice);
    }
    
}
