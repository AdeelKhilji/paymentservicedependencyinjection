/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Adeel Khilji
 */
public class DiscountFactory 
{
    private static DiscountFactory discountFactory;
    
    private DiscountFactory(){}
    
    public static DiscountFactory getInstance()
    {
        if(discountFactory == null)
        {
            discountFactory = new DiscountFactory();
        }
        return discountFactory;
    }
    
    public Discount getDiscount(DiscountType type)
    {
        Discount discount = null;
        switch(type)
        {
            case AMOUNT: discount = new DiscountByAmount();
            break;
            
            case PERCENT: discount = new DiscountByPercentage();
            break;
        }
        return discount;
    }
}
