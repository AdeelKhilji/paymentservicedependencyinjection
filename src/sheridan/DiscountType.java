/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Adeel Khilji
 */
public enum DiscountType 
{
    AMOUNT("$"),PERCENT("%");
    
    public String displayName;
    DiscountType(String displayName)
    {
        this.displayName = displayName;
    }
    public String toString()
    {
        return displayName;
    }
}
