/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan;

/**
 *
 * @author Adeel Khilji
 */
public class ShoppingCartDemo 
{
    public static void main(String[] args)
    {
        PaymentServiceFactory paymentFactory = PaymentServiceFactory.getInstance();
        PaymentService creditService = paymentFactory.getPaymentService(PaymentServiceType.CREDIT);
        PaymentService debitService = paymentFactory.getPaymentService(PaymentServiceType.DEBIT);
        
        DiscountFactory discountFactory = DiscountFactory.getInstance();
        Discount discountByAmount = discountFactory.getDiscount(DiscountType.AMOUNT);
        Discount discountByPercentage = discountFactory.getDiscount(DiscountType.PERCENT);
        
        
        Cart cart = new Cart();
        cart.addProduct( new Product("shirt", 50));
        cart.addProduct( new Product("pants", 60));
        discountByAmount.calculateDiscount(10.00);
        discountByPercentage.calculateDiscount(20.00);
        cart.setPaymentService(creditService);
        cart.payCart();
        
        cart.setPaymentService(debitService);
        cart.payCart();
        
        
        
    }
}
